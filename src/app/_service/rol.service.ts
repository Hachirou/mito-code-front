/*
import { HOST, TOKEN_NAME, MICRO_CRUD } from './../_shared/var.constant';
import { Injectable } from '@angular/core';
import { HttpClient } from  '@angular/common/http';
import { Rol} from '../_model/rol';

@Injectable({
  providedIn: 'root'
})
export class RolService {

  constructor(private http: HttpClient) { }

  url: string = `${HOST}/ROLES`;


  listar (){
    return this.http.get<Rol>(this.url);
  }

}

*/
import { HttpClient } from '@angular/common/http';
import { HOST, MICRO_CRUD } from './../_shared/var.constant';
import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { Rol } from '../_model/rol';

@Injectable({
  providedIn: 'root'
})
export class RolService {

  rolesCambio = new Subject<Rol[]>();
  mensajeCambio = new Subject<string>();

  url: string = `${HOST}/roles`;

  constructor(private http: HttpClient) { }

  listar() {
    return this.http.get<Rol[]>(this.url);
  }

  listarRolPorId(id: number) {
    return this.http.get<Rol>(`${this.url}/${id}`);
  }
  
  listarRolPorMenu(id: number) {
    return this.http.get<Rol>(`${this.url}/${id}`);
  }

  registrar(rol: Rol) {
    return this.http.post(this.url, rol);
  }

  modificar(rol: Rol) {
    return this.http.put(this.url, rol);
  }

  eliminar(id: number) {
    return this.http.delete(`${this.url}/${id}`);
  }
}
