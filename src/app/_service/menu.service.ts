import { Subject } from 'rxjs';
import { Menu } from './../_model/menu';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HOST, TOKEN_NAME, MICRO_CR } from './../_shared/var.constant';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class MenuService {

  menuCambio = new Subject<Menu[]>();
  mensajeCambio = new Subject<string>();
  
  private url: string = `${HOST}`;
  private url2: string = `${HOST}/menus/`;
  //private url: string = `${HOST}/${MICRO_CR}`;

  constructor(private http: HttpClient) { }

  listar() {
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.get<Menu[]>(`${this.url}/menus`, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }

  listarPorUsuario(nombre: string) {
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.post<Menu[]>(`${this.url}/menus/usuario`, nombre, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }

  
  listarMenuPorId(id: number) {
      return this.http.get<Menu>(`${this.url2}${id}`);
  }

  registrar(menu: Menu) {
    return this.http.post(this.url2, menu);
  }
  modificar(menu: Menu) {
    return this.http.put(this.url2, menu);
  }

  eliminar(id: number) {
    console.log(`${this.url2}${id}`);
    return this.http.delete(`${this.url2}${id}`);
  }
}
