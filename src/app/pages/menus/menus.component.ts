import { ActivatedRoute, Router, Params } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit ,ViewChild} from '@angular/core';
import { Menu } from 'src/app/_model/menu';
import { MenuService } from 'src/app/_service/menu.service';
import { MatSort, MatPaginator, MatTableDataSource, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-menus',
  templateUrl: './menus.component.html',
  styleUrls: ['./menus.component.css']
})
export class MenusComponent implements OnInit {

  id: number;
  menu: Menu;
  form: FormGroup;
  edicion: boolean = false;

  displayedColumns = ['id', 'nombre','icono', 'url', 'acciones'];
  dataSource: MatTableDataSource<Menu>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private menuService: MenuService, private route: ActivatedRoute, private router: Router) {
    this.menu = new Menu();
    this.form = new FormGroup({
      'id': new FormControl(0),
      'nombre': new FormControl(''),
      'icono': new FormControl(''),
      'url': new FormControl('')
    });
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
    });

    this.menuService.menuCambio.subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    this.menuService.listar().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });    
  }

  initForm(){
    if (this.edicion) {
      this.menuService.listarMenuPorId(this.id).subscribe(data => {
        let id = data.idMenu;
        let nombre = data.nombre;
        let icono = data.icono;
        let url = data.url
        this.form = new FormGroup({
          'id': new FormControl(id),
          'nombre': new FormControl(nombre),
          'icono ': new FormControl(icono),
          'url ': new FormControl(url)
        });
      });
    }
  }

  operar() {
    this.menu.idMenu = this.form.value['id'];
    this.menu.nombre = this.form.value['nombre'];
    this.menu.icono = this.form.value['icono'];
    this.menu.url = this.form.value['url'];

    if (this.menu != null && this.menu.idMenu> 0) {
      this.menuService.modificar(this.menu).subscribe(data => {
        this.menuService.listar().subscribe(rol => {
          this.menuService.menuCambio.next(rol);
          this.menuService.mensajeCambio.next("Se modificó");
        });
      });
    } else {
      this.menuService.registrar(this.menu).subscribe(data => {
        console.log(data);
        this.menuService.listar().subscribe(menu => {
          this.menuService.menuCambio.next(menu);
          this.menuService.mensajeCambio.next("Se registró");
        });
      });
    }

    this.router.navigate(['menu']);
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); 
    filterValue = filterValue.toLowerCase(); 
    this.dataSource.filter = filterValue;
  }

  eliminar(idMenu: number){
    this.menuService.eliminar(idMenu).subscribe(data => {      
      this.menuService.listar().subscribe(data => {
        this.menuService.menuCambio.next(data);
        this.menuService.mensajeCambio.next('Se eliminó');
      });      
    });
  }

}

