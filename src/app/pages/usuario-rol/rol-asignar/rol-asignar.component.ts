/*
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-rol-asignar',
  templateUrl: './rol-asignar.component.html',
  styleUrls: ['./rol-asignar.component.css']
})
export class RolAsignarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
*/
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Rol } from 'src/app/_model/rol';
import { RolService } from 'src/app/_service/rol.service';

import { FormGroup, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { UsuarioService } from 'src/app/_service/usuario.service';
import { Usuario } from 'src/app/_model/usuario';

@Component({
  selector: 'app-rol-asignar',
  templateUrl: './rol-asignar.component.html',
  styleUrls: ['./rol-asignar.component.css']
})
export class RolAsignarComponent implements OnInit {

  roles : Rol[] = [];
  rolesExistentes : Rol[] = [];
  rolesSeleccionados : Rol[] = [];

  mensaje :string;

  idRolSeleccionado: number;
  idUsuario : number;
  usuarioSeleccionado : String = "Sin Asignar Menu" ;


  form: FormGroup;

  
  constructor(private usuarioService:UsuarioService, private rolService:RolService,private route: ActivatedRoute, private router: Router,private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.idUsuario = params['id'];
      this.initForm();
    });
    
    this.listarRoles();
    this.listarRolesPorMenu();
  }

  listarRoles() {
    this.rolService.listar().subscribe(data => {
      this.roles = data;
    });
  }
  
  listarRolesPorMenu() {
    console.log(this.idUsuario);
    this.usuarioService.listarUsuarioPorId(this.idUsuario).subscribe(data => {
      console.log(data.roles);
      this.rolesExistentes = data.roles;
    });
  }

  initForm(){
      this.usuarioService.listarUsuarioPorId(this.idUsuario).subscribe(data => {
        let idMenuSeleccionado = data.idUsuario;
        this.usuarioSeleccionado = data.username;
      });
    
  }

  removerRol(index: number) {
    this.rolesSeleccionados.splice(index, 1);
  }

  agregarRol() {
    if (this.idRolSeleccionado > 0) {

      let cont = 0;
      for (let i = 0; i < this.rolesSeleccionados.length; i++) {
        let rol = this.rolesSeleccionados[i];
        if (rol.idRol === this.idRolSeleccionado) {
          cont++;
          break;
        }
      }

      if (cont > 0) {
        this.mensaje = `El ROL ya se encuentra en la lista`;
        this.snackBar.open(this.mensaje, "Aviso", { duration: 2000 });
      } else {
        let rol = new Rol();
        rol.idRol= this.idRolSeleccionado;
        this.rolService.listarRolPorId(this.idRolSeleccionado).subscribe(data => {
          rol.nombre = data.nombre;
          this.rolesSeleccionados.push(rol);
        });
      }
    } else {
      this.mensaje = `Debe agregar un Rol`;
      this.snackBar.open(this.mensaje, "Aviso", { duration: 2000 });
    }
  }

  estadoBotonRegistrar() {
    return (this.rolesSeleccionados.length === 0);
  }

  aceptar() {
    let usuario = new Usuario();
    usuario.idUsuario = this.idUsuario;

    this.usuarioService.listarUsuarioPorId(this.idUsuario).subscribe(data => {
      console.log('Clave:');
      console.log(data.password);
      usuario.password = data.password;
      usuario.username = data.username; 
      usuario.enabled = data.enabled; 
    });
    usuario.roles = this.rolesSeleccionados;

    console.log(usuario);

    this.usuarioService.registrar(usuario).subscribe(() => {
      this.snackBar.open("Se registró", "Aviso", { duration: 2000 });

      setTimeout(() => {
        this.router.navigate(['usuarioRol']);
      }, 2000);
    });
    //(console.log(consulta);
  }

}
