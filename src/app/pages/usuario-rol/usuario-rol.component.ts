/*import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-usuario-rol',
  templateUrl: './usuario-rol.component.html',
  styleUrls: ['./usuario-rol.component.css']
})
export class UsuarioRolComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
*/

import { ActivatedRoute, Router, Params } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit ,ViewChild} from '@angular/core';
import { UsuarioService } from 'src/app/_service/usuario.service';
import { Usuario } from 'src/app/_model/usuario';
import { MatSort, MatPaginator, MatTableDataSource, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-usuario-rol',
  templateUrl: './usuario-rol.component.html',
  styleUrls: ['./usuario-rol.component.css']
})
export class UsuarioRolComponent implements OnInit {

  id: number;
  usuario: Usuario;
  form: FormGroup;
  edicion: boolean = false;

  displayedColumns = ['id', 'username', 'acciones'];
  dataSource: MatTableDataSource<Usuario>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private usuarioService: UsuarioService, private route: ActivatedRoute, private router: Router) {
    this.usuario = new Usuario();
    this.form = new FormGroup({
      'id': new FormControl(0),
      'username': new FormControl(''),
    });
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
    });

    this.usuarioService.usuarioCambio.subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    this.usuarioService.listar().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });    
  }

  initForm(){
    if (this.edicion) {
      this.usuarioService.listarUsuarioPorId(this.id).subscribe(data => {
        let id = data.idUsuario;
        let username = data.username;
        this.form = new FormGroup({
          'id': new FormControl(id),
          'username': new FormControl(username)
        });
      });
    }
  }

  operar() {
    this.usuario.idUsuario = this.form.value['id'];
    this.usuario.username = this.form.value['username'];

    if (this.usuario != null && this.usuario.idUsuario> 0) {
      this.usuarioService.modificar(this.usuario).subscribe(data => {
        this.usuarioService.listar().subscribe(usuario => {
          this.usuarioService.usuarioCambio.next(usuario);
          this.usuarioService.mensajeCambio.next("Se modificó");
        });
      });
    } else {
      this.usuarioService.registrar(this.usuario).subscribe(data => {
        console.log(data);
        this.usuarioService.listar().subscribe(usuario => {
          this.usuarioService.usuarioCambio.next(usuario);
          this.usuarioService.mensajeCambio.next("Se registró");
        });
      });
    }

    this.router.navigate(['usuarioRol']);
  }

  

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); 
    filterValue = filterValue.toLowerCase(); 
    this.dataSource.filter = filterValue;
  }

}


