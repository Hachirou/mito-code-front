import { ActivatedRoute, Router, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Rol } from 'src/app/_model/rol';
import { Menu } from 'src/app/_model/menu';
import { RolService } from 'src/app/_service/rol.service';

import { MenuService } from 'src/app/_service/menu.service';
import { FormGroup, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-menu-asignar',
  templateUrl: './menu-asignar.component.html',
  styleUrls: ['./menu-asignar.component.css']
})
export class MenuAsignarComponent implements OnInit {

  roles : Rol[] = [];
  rolesExistentes : Rol[] = [];
  rolesSeleccionados : Rol[] = [];

  mensaje :string;

  idRolSeleccionado: number;
  idMenu : number;
  menuSeleccionado : String = "Sin Asignar Menu" ;


  form: FormGroup;

  
  constructor(private menuService:MenuService, private rolService:RolService,private route: ActivatedRoute, private router: Router,private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.idMenu = params['id'];
      this.initForm();
    });
    
    this.listarRoles();
    this.listarRolesPorMenu();
  }

  listarRoles() {
    this.rolService.listar().subscribe(data => {
      this.roles = data;
    });
  }
  
  listarRolesPorMenu() {
    console.log(this.idMenu);
    this.menuService.listarMenuPorId(this.idMenu).subscribe(data => {
      console.log(data.roles);
      this.rolesExistentes = data.roles;
    });
  }

  initForm(){
      this.menuService.listarMenuPorId(this.idMenu).subscribe(data => {
        let idMenuSeleccionado = data.idMenu;
        this.menuSeleccionado = data.nombre;
      });
    
  }

  removerRol(index: number) {
    this.rolesSeleccionados.splice(index, 1);
  }

  agregarRol() {
    if (this.idRolSeleccionado > 0) {

      let cont = 0;
      for (let i = 0; i < this.rolesSeleccionados.length; i++) {
        let rol = this.rolesSeleccionados[i];
        if (rol.idRol === this.idRolSeleccionado) {
          cont++;
          break;
        }
      }

      if (cont > 0) {
        this.mensaje = `El examen se encuentra en la lista`;
        this.snackBar.open(this.mensaje, "Aviso", { duration: 2000 });
      } else {
        let rol = new Rol();
        rol.idRol= this.idRolSeleccionado;
        this.rolService.listarRolPorId(this.idRolSeleccionado).subscribe(data => {
          rol.nombre = data.nombre;
          this.rolesSeleccionados.push(rol);
        });
      }
    } else {
      this.mensaje = `Debe agregar un Rol`;
      this.snackBar.open(this.mensaje, "Aviso", { duration: 2000 });
    }
  }

  estadoBotonRegistrar() {
    return (this.rolesSeleccionados.length === 0);
  }

  aceptar() {
    let menu = new Menu();
    menu.idMenu = this.idMenu;
    this.menuService.listarMenuPorId(this.idMenu).subscribe(data => {
      console.log('data');
      console.log(data.icono);
      menu.icono = data.icono;
      menu.nombre = data.nombre; 
      menu.url = data.url; 

      menu.roles = this.rolesSeleccionados;

      this.menuService.registrar(menu).subscribe(() => {
        this.snackBar.open("Se registró", "Aviso", { duration: 2000 });
  
        setTimeout(() => {
          this.router.navigate(['menuRol']);
        }, 2000);
      });
    });
    //(console.log(consulta);
  }

}
