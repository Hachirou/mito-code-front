/*import { Component, OnInit } from '@angular/core';
import { RolService } from 'src/app/_service/rol.service';

@Component({
  selector: 'app-rol',
 templateUrl: './rol.component.html',
 styleUrls: ['./rol.component.css']
})
export class RolComponent implements OnInit {

  constructor(private rolService : RolService ) {

  }

    ngOnInit() {
      this.rolService.listar();
      
  }

}

*/

import { ActivatedRoute } from '@angular/router';
import { RolService } from './../../_service/rol.service';
import { Rol } from './../../_model/rol';
import { MatSort, MatPaginator, MatTableDataSource, MatSnackBar } from '@angular/material';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-rol',
  templateUrl: './rol.component.html',
  styleUrls: ['./rol.component.css']
})
export class RolComponent implements OnInit {

  displayedColumns = ['id', 'nombre', 'descripcion','acciones'];
  dataSource: MatTableDataSource<Rol>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private rolService: RolService, public route: ActivatedRoute, private snackBar: MatSnackBar) { }

  ngOnInit() {    
    this.rolService.rolesCambio.subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    this.rolService.listar().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });    
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); 
    filterValue = filterValue.toLowerCase(); 
    this.dataSource.filter = filterValue;
  }

  eliminar(idRol: number){
    this.rolService.eliminar(idRol).subscribe(data => {      
      this.rolService.listar().subscribe(data => {
        this.rolService.rolesCambio.next(data);
        this.rolService.mensajeCambio.next('Se eliminó');
      });      
    });
  }

}
